#ifndef COMMON_H
#define COMMON_H


//#define USB_DEBUG
#ifdef USB_DEBUG
#define FPS (10)
#else
#define FPS (60)
#endif

#define DEVICE_ID 0x01
#define CLIENT1_DEVICE_ID 0x02
#define CLIENT2_DEVICE_ID 0x03
#define CLIENT_QTY 2

#define RENDA_MAX 32

#define NETWORK_TX_NAME PA_9
#define NETWORK_RX_NAME PA_10
#define NETWORK_RTS_NAME D12

#define MATRIX_SDA D4
#define MATRIX_SCL D5
#define MATRIX_ADDRESS 0x70


//DEFAULT VALUE
//#define MY_BOUDRATE 9600

//SAFE MODE
#define MY_BOUDRATE (115200)


//#define MY_BOUDRATE (512*1000)

//OK
//#define MY_BOUDRATE (1000*1000)


#define USB_SERIAL_BOUDRATE (115200)

#endif//COMMON_H
