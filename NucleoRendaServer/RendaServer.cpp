#include "RendaServer.h"
#include "PacketGetCount.h"
#include "PacketConverter.h"


RendaServer::RendaServer():
BinaryPacketDispatcherCallbackInterface(),
mClientStates(),
mPollingClientIndex(0),
mI2C(MATRIX_SDA, MATRIX_SCL),
mMatrix(&mI2C),
mCurrentState(NULL),
mNextState(NULL),
#ifdef USB_DEBUG
mUSB(USBTX, USBRX,USB_SERIAL_BOUDRATE),
mDebugTimer(),
#endif
mTextPacketParser(),
mBinaryPacketParser(),
mBinaryPacketDispatcher(DEVICE_ID),
mTicker(),
mTimeout(),
mNetwork(NETWORK_TX_NAME,NETWORK_RX_NAME,MY_BOUDRATE),
mSendingBuffer(),
mReceivingBuffer(),
mSendState(SendState_Idle),
mRTS(NETWORK_RTS_NAME),
mLED(LED1){
    mClientStates[0].mDeviceId=CLIENT1_DEVICE_ID;
    mClientStates[1].mDeviceId=CLIENT2_DEVICE_ID;
    mTextPacketParser.assign(&mBinaryPacketParser);
    mBinaryPacketParser.assign(&mBinaryPacketDispatcher);
    mBinaryPacketDispatcher.assign(this);
    setup();
}
void RendaServer::setup(){
    mMatrix.begin(MATRIX_ADDRESS);
    mRTS=0;
    mTicker.attach(callback(this,&RendaServer::onTick),1.0/FPS);
    mNetwork.attach(callback(this, &RendaServer::onReceive),Serial::RxIrq);
    
    mCurrentState=new RendaGameStateTitle(this);
    mCurrentState->onBegin();
}
void RendaServer::update(){
    if(mNextState){
        mCurrentState->onEnd();
        delete mCurrentState;
        mCurrentState=mNextState;
        mNextState=NULL;
        mCurrentState->onBegin();
    }
    receiveIfNeeded();
    sendIfNeeded();
    if(mNeedUpdateMatrix){
        mNeedUpdateMatrix=false;
        updateMatrix();
    }
   
}
void RendaServer::onPacketGetCountCommand(const PacketGetCountCommand& inCommand){
    //DO NOTHING
}
void RendaServer::onPacketGetCountReply(const PacketGetCountReply& inReply){
    mLED=!mLED;
    for(int i=0;i<CLIENT_QTY;++i){
        ClientState& clientState=mClientStates[i];
        if(clientState.mDeviceId==inReply.header.from){
            mCurrentState->onUpdateClientCount(i,inReply.count);
        }
    }
    
    
}
void RendaServer::onTick(){
    mNeedUpdateMatrix=true;
    sendGetCountCommand();
}
void RendaServer::onReceive(){
    while(0<mNetwork.readable()){
        int c=mNetwork.getc();
        mReceivingBuffer.put((char)c);
    }
}

void RendaServer::sendGetCountCommand(){
    if(mSendState!=SendState_Idle){
#ifdef USB_DEBUG
        mUSB.puts("[not idle]");
#endif
        return;
    }
    int clientDeviceId=mClientStates[mPollingClientIndex].mDeviceId;
    mPollingClientIndex=(mPollingClientIndex+1)%CLIENT_QTY;
    
    PacketGetCountCommand command;
    memset(&command,0,sizeof(command));
    command.header.from=DEVICE_ID;
    command.header.to=clientDeviceId;
    command.header.command=COMMAND_GET_COUNT;
    command.header.size=sizeof(command);
    command.header.bcc=PacketConverter::calcBCC((unsigned char*)&command,sizeof(command));
    std::vector<unsigned char> binaryBuffer;
    binaryBuffer.resize(sizeof(command));
    memcpy(&binaryBuffer[0],&command,sizeof(command));
    
    std::vector<char> textBuffer;
    PacketConverter::convertBinaryPacketToTextPacket(binaryBuffer,textBuffer);
    mSendingBuffer.put(TextPacketParser::STX);
#ifdef USB_DEBUG
    mUSB.puts("S[");
#endif
    for(int i=0;i<textBuffer.size();++i){
        char t=textBuffer[i];
        mSendingBuffer.put(t);
#ifdef USB_DEBUG
        mUSB.putc(t);
#endif
    }
    mSendingBuffer.put(TextPacketParser::ETX);
#ifdef USB_DEBUG
    mUSB.puts("]\r\n");
#endif
    
}
void RendaServer::receiveIfNeeded(){
    while(mReceivingBuffer.available()){
        char c=mReceivingBuffer.get();
#ifdef USB_DEBUG
        switch(c){
        case 0x02:
            mUSB.puts("R[");
            break;
        case 0x03:
            mUSB.puts("]\r\n");
            break;
        default:
            mUSB.putc(c);
            break;
        }
#endif
        mTextPacketParser.write(c);
    }
}

void RendaServer::sendIfNeeded(){
    if( mNetwork.writeable()){
        switch(mSendState){
        case SendState_Idle:
            if(mSendingBuffer.available()){
                mSendState=SendState_Sending;
                mRTS=1;
#ifdef USB_DEBUG
                mDebugTimer.reset();
                mDebugTimer.start();
#endif
                wait_us(100);//TODO
            }
            break;
        case SendState_Sending:
            if(mSendingBuffer.available()){
                char c=mSendingBuffer.get();
                int result=mNetwork.putc(c);
#ifdef USB_DEBUG
                wait_us(10);
#else
                wait_us(100);
#endif

                //Dont need wait if rxIrq
                //wait_us(100);//OK IF DIABLE CLIENT USB_DEBUG
                //wait_us(800);//OK
            }else{
                mSendState=SendState_Idle;
#ifdef USB_DEBUG
                wait_us(100);
#else
#endif
                mRTS=0;
#ifdef USB_DEBUG
                mDebugTimer.stop();
                mUSB.printf("(%f)\r\n",mDebugTimer.read());
#endif

            }
            break;
        default:
            //DO NOTHING
            break;
        }
    }
}

void RendaServer::updateMatrix(){
    
    mCurrentState->onUpdateMatrix();
}

void RendaServer::setNextState(RendaGameStateBase* inNextState){
    mNextState=inNextState;
}

int RendaServer::getClientTotalCount(size_t inIndex){
    if(CLIENT_QTY<=inIndex){
        return 0;
    }
    return mClientStates[inIndex].mButtonCounter.mTotalCount;
}
void RendaServer::clearClientTotalCount(size_t inIndex){
    if(CLIENT_QTY<=inIndex){
        return;
    }
    mClientStates[inIndex].mButtonCounter.mTotalCount=0;
}
void RendaServer::updateClientCount(size_t inIndex,unsigned char inCount,bool inIsCountUp){
    if(CLIENT_QTY<=inIndex){
        return;
    }
    mClientStates[inIndex].mButtonCounter.update(inCount,inIsCountUp);
}
Adafruit_8x8matrix* RendaServer::getMatrix(){
    return &mMatrix;
}





