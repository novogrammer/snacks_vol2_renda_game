#ifndef TEXT_PACKET_PARSER_H
#define TEXT_PACKET_PARSER_H

#include<vector>

struct TextPacketParserCallbackInterface{
    virtual void onTextPacket(const std::vector<char>& inBuffer)=0;
};

class TextPacketParserTest;

//[STX]text[ETX]
class TextPacketParser{
    friend class TextPacketParserTest;
    std::vector<char> mBuffer;
    enum BufferState{
        BufferState_WaitForSTX,
        BufferState_WaitForETX,
    }mBufferState;
    TextPacketParserCallbackInterface* mCallback;
public:
    static const char STX;
    static const char ETX;
    
    TextPacketParser();
    void assign(TextPacketParserCallbackInterface* inCallback);
    
    void write(char inIncommingCharactor);


};

#endif//TEXT_PACKET_PARSER_H

