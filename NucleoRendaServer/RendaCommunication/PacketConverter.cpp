#include "PacketConverter.h"

void PacketConverter::makeResponseHeader(const PacketHeader* commandHeader,PacketHeader* replyHeader){
    replyHeader->from=commandHeader->to;
    replyHeader->to=commandHeader->from;
    replyHeader->command=(commandHeader->command | COMMAND_REPLY_MODIFIER);
}


const char HEX[16]={
    '0','1','2','3',
    '4','5','6','7',
    '8','9','a','b',
    'c','d','e','f',
};


void PacketConverter::convertTextPacketToBinaryPacket(const std::vector<char>& inTextPacket,std::vector<unsigned char>& outBinaryPacket){
    outBinaryPacket.clear();
    outBinaryPacket.resize((inTextPacket.size()+1)/2);
    for(int i=0;i<inTextPacket.size();++i){
        char t=inTextPacket[i];
        unsigned char b=0;
        for(int j=0;j<16;++j){
            if(HEX[j]==(t|0x20)){
                b=j;
                break;
            }
        }
        outBinaryPacket[i/2]|=b<<(i%2==0?4:0);
    }
}
void PacketConverter::convertBinaryPacketToTextPacket(const std::vector<unsigned char>& inBinaryPacket,std::vector<char>& outTextPacket){
    outTextPacket.clear();
    outTextPacket.resize(inBinaryPacket.size()*2);
    for(int i=0;i<inBinaryPacket.size();++i){
        unsigned char b=inBinaryPacket[i];
        outTextPacket[i*2+0]=HEX[ b>>4 & 0xf ];
        outTextPacket[i*2+1]=HEX[ b>>0 & 0xf ];
    }
}

