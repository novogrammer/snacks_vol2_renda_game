#include "TextPacketParser.h"
#include "PacketHeader.h"
#include "PacketGetCount.h"
#include "PacketConverter.h"
#include "BinaryPacketParser.h"
#include "BinaryPacketDispatcher.h"

#include "RendaGameState.h"

#include "common.h"
#include "mbed.h"
#include "MyBuffer.h"
#include "Adafruit_LEDBackpack.h"


class RendaServer:public BinaryPacketDispatcherCallbackInterface,public RendaGameContextInterface{
    struct ButtonCounter{
        int mTotalCount;
        unsigned char mPreviousCount;
        ButtonCounter():mTotalCount(),mPreviousCount(){
        }
        void update(unsigned char inCount,bool inIsCountUp=true){
            unsigned char diff=inCount-mPreviousCount;
            if(inIsCountUp){
                mTotalCount+=(int)diff;
            }
            mPreviousCount=inCount;
        }
    };
    struct ClientState{
        unsigned char mDeviceId;
        ButtonCounter mButtonCounter;
        ClientState():mDeviceId(),mButtonCounter(){
        }
    }mClientStates[CLIENT_QTY];
    size_t mPollingClientIndex;
    I2C mI2C;
    Adafruit_8x8matrix mMatrix;
    bool mNeedUpdateMatrix;
    
    RendaGameStateBase* mCurrentState;
    RendaGameStateBase* mNextState;
    
#ifdef USB_DEBUG
    Serial mUSB;
    Timer mDebugTimer;
#endif
    TextPacketParser mTextPacketParser;
    BinaryPacketParser mBinaryPacketParser;
    BinaryPacketDispatcher mBinaryPacketDispatcher;
    Ticker mTicker;
    Timeout mTimeout;
    RawSerial mNetwork;
    MyBuffer<char> mSendingBuffer;
    MyBuffer<char> mReceivingBuffer;
    enum SendState{
        SendState_Idle,
        SendState_Sending,
    }mSendState;
    DigitalOut mRTS;
    DigitalOut mLED;
public:
    RendaServer();
    void setup();
    void update();
    virtual void onPacketGetCountCommand(const PacketGetCountCommand& inCommand);
    virtual void onPacketGetCountReply(const PacketGetCountReply& inReply);
    void onTick();
    void onReceive();
    void sendGetCountCommand();
    void receiveIfNeeded();
    void sendIfNeeded();
    void updateMatrix();
    virtual void setNextState(RendaGameStateBase* inNextState);
    virtual int getClientTotalCount(size_t inIndex);
    virtual void clearClientTotalCount(size_t inIndex);
    virtual void updateClientCount(size_t inIndex,unsigned char inCount,bool inIsCountUp);
    virtual Adafruit_8x8matrix* getMatrix();
};

