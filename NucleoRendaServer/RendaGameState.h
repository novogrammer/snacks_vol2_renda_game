#ifndef RENDA_GAME_STATE_H
#define RENDA_GAME_STATE_H
#include "common.h"

#include "mbed.h"

//Don't call these from interrupt handler!!

class Adafruit_8x8matrix;
class RendaGameStateBase;

struct RendaGameContextInterface{
    virtual void setNextState(RendaGameStateBase* inNextState)=0;
    virtual int getClientTotalCount(size_t inIndex)=0;
    virtual void clearClientTotalCount(size_t inIndex)=0;
    virtual void updateClientCount(size_t inIndex,unsigned char inCount,bool inIsCountUp)=0;
    virtual Adafruit_8x8matrix* getMatrix()=0;
    
};


class RendaGameStateBase{
protected:
    RendaGameContextInterface* mContext;
    RendaGameStateBase(RendaGameContextInterface* inContext):mContext(inContext){
    }
    void updateMatrixForBattle();
public:
    virtual void onBegin()=0;
    virtual void onEnd()=0;
    virtual void onUpdateClientCount(size_t inIndex,unsigned char inCount)=0;
    virtual void onUpdateMatrix()=0;
};


class RendaGameStateTitle:public RendaGameStateBase{
    int mAnimationCount;
public:
    RendaGameStateTitle(RendaGameContextInterface* inContext):RendaGameStateBase(inContext),mAnimationCount(0){
    }
    virtual void onBegin();
    virtual void onEnd();
    virtual void onUpdateClientCount(size_t inIndex,unsigned char inCount);
    virtual void onUpdateMatrix();
};

class RendaGameStateCountdown:public RendaGameStateBase{
    int mAnimationCount;
public:
    RendaGameStateCountdown(RendaGameContextInterface* inContext):RendaGameStateBase(inContext),mAnimationCount(0){
    }
    virtual void onBegin();
    virtual void onEnd();
    virtual void onUpdateClientCount(size_t inIndex,unsigned char inCount);
    virtual void onUpdateMatrix();
};

class RendaGameStateBattle:public RendaGameStateBase{
public:
    RendaGameStateBattle(RendaGameContextInterface* inContext):RendaGameStateBase(inContext){
    }
    virtual void onBegin();
    virtual void onEnd();
    virtual void onUpdateClientCount(size_t inIndex,unsigned char inCount);
    virtual void onUpdateMatrix();
};

class RendaGameStateEnding:public RendaGameStateBase{
    int mAnimationCount;
public:
    RendaGameStateEnding(RendaGameContextInterface* inContext):RendaGameStateBase(inContext),mAnimationCount(0){
    }
    virtual void onBegin();
    virtual void onEnd();
    virtual void onUpdateClientCount(size_t inIndex,unsigned char inCount);
    virtual void onUpdateMatrix();
};



#endif//RENDA_GAME_STATE_H

