#include "RendaGameState.h"
#include "Adafruit_LEDBackpack.h"

#define BIT_PATTERN(a,b,c,d,e,f,g,h) (uint8_t)((a<<7)|(b<<6)+(c<<5)+(d<<4)+(e<<3)+(f<<2)+(g<<1)+(h<<0))
static const uint8_t
winner_right_01_bmp[]=
{
BIT_PATTERN(0,0,0,0,0,1,0,0),
BIT_PATTERN(0,0,0,1,1,1,1,1),
BIT_PATTERN(0,0,0,0,1,1,1,0),
BIT_PATTERN(0,0,0,1,0,0,0,1),
BIT_PATTERN(0,0,0,1,1,1,1,1),
BIT_PATTERN(0,0,0,0,1,1,1,0),
BIT_PATTERN(0,0,0,0,0,1,0,0),
BIT_PATTERN(0,0,0,0,1,1,1,0),
},
winner_right_02_bmp[]=
{
BIT_PATTERN(0,0,0,0,0,0,0,0),
BIT_PATTERN(0,0,0,0,0,0,0,0),
BIT_PATTERN(0,0,0,0,0,0,0,0),
BIT_PATTERN(0,0,0,0,0,0,0,0),
BIT_PATTERN(0,0,0,1,1,1,1,1),
BIT_PATTERN(0,0,0,0,1,1,1,0),
BIT_PATTERN(0,0,0,0,0,1,0,0),
BIT_PATTERN(0,0,0,0,1,1,1,0),
},
winner_left_01_bmp[]=
{
BIT_PATTERN(0,0,1,0,0,0,0,0),
BIT_PATTERN(1,1,1,1,1,0,0,0),
BIT_PATTERN(0,1,1,1,0,0,0,0),
BIT_PATTERN(1,0,0,0,1,0,0,0),
BIT_PATTERN(1,1,1,1,1,0,0,0),
BIT_PATTERN(0,1,1,1,0,0,0,0),
BIT_PATTERN(0,0,1,0,0,0,0,0),
BIT_PATTERN(0,1,1,1,0,0,0,0),
},
winner_left_02_bmp[]=
{
BIT_PATTERN(0,0,0,0,0,0,0,0),
BIT_PATTERN(0,0,0,0,0,0,0,0),
BIT_PATTERN(0,0,0,0,0,0,0,0),
BIT_PATTERN(0,0,0,0,0,0,0,0),
BIT_PATTERN(1,1,1,1,1,0,0,0),
BIT_PATTERN(0,1,1,1,0,0,0,0),
BIT_PATTERN(0,0,1,0,0,0,0,0),
BIT_PATTERN(0,1,1,1,0,0,0,0),
};

//RendaGameStateBase
void RendaGameStateBase::updateMatrixForBattle(){
    Adafruit_8x8matrix* matrix=mContext->getMatrix();
    matrix->clear();
    matrix->setRotation(1);
    for(int ic=0;ic<CLIENT_QTY;++ic){
        int count=mContext->getClientTotalCount(ic);
        int ledCount=0;
        if(ic==0){
            for(int16_t x=8-1;4<=x;--x){
                for(int16_t y=0;y<8;++y){
                    if(ledCount<count){
                        matrix->drawPixel(x, y, LED_ON);
                    }
                    ++ledCount;
                }
            }
        }else{
            for(int16_t x=0;x<4;++x){
                for(int16_t y=0;y<8;++y){
                    if(ledCount<count){
                        matrix->drawPixel(x, y, LED_ON);
                    }
                    ++ledCount;
                }
            }
        }
    }
    matrix->writeDisplay();
}


//RendaGameStateTitle
void RendaGameStateTitle::onBegin(){
    for(size_t i=0;i<CLIENT_QTY;++i){
        mContext->clearClientTotalCount(i);
    }
}
void RendaGameStateTitle::onEnd(){
}
void RendaGameStateTitle::onUpdateClientCount(size_t inIndex,unsigned char inCount){
    mContext->updateClientCount(inIndex,inCount,true);
    if(0<mContext->getClientTotalCount(inIndex)){
        mContext->setNextState(new RendaGameStateCountdown(mContext));
    }
}
void RendaGameStateTitle::onUpdateMatrix(){
    static const char* str="RENDA GAME PRESS BUTTON!";
    Adafruit_8x8matrix* matrix=mContext->getMatrix();
    int len=strlen(str);
    int x=-mAnimationCount/4+10;
    matrix->clear();
    matrix->setRotation(3);
    for(int ic=0;ic<len;++ic){
        char c=str[ic];
        matrix->drawChar(x+ic*6,1,c,LED_ON,LED_OFF,1);
    }
    matrix->writeDisplay();

    mAnimationCount=(mAnimationCount+1)%(160*4);
}


//RendaGameStateCountdown
void RendaGameStateCountdown::onBegin(){
}
void RendaGameStateCountdown::onEnd(){
}
void RendaGameStateCountdown::onUpdateClientCount(size_t inIndex,unsigned char inCount){
    //avoid count up
    mContext->updateClientCount(inIndex,inCount,false);
}
void RendaGameStateCountdown::onUpdateMatrix(){
    Adafruit_8x8matrix* matrix=mContext->getMatrix();
    matrix->clear();
    matrix->setRotation(3);
    switch(mAnimationCount/FPS){
    case 0:
        matrix->drawChar(2,1,'3',LED_ON,LED_OFF,1);
        break;
    case 1:
        matrix->drawChar(2,1,'2',LED_ON,LED_OFF,1);
        break;
    case 2:
        matrix->drawChar(2,1,'1',LED_ON,LED_OFF,1);
        break;
    default:
        //DO NOTHING
        break;
    }
    matrix->writeDisplay();
    
    ++mAnimationCount;
    if(FPS*3<mAnimationCount){
        mContext->setNextState(new RendaGameStateBattle(mContext));
    }
}


//RendaGameStateBattle
void RendaGameStateBattle::onBegin(){
    for(size_t i=0;i<CLIENT_QTY;++i){
        mContext->clearClientTotalCount(i);
    }
}
void RendaGameStateBattle::onEnd(){
}
void RendaGameStateBattle::onUpdateClientCount(size_t inIndex,unsigned char inCount){
    mContext->updateClientCount(inIndex,inCount,true);
    if(RENDA_MAX<=mContext->getClientTotalCount(inIndex)){
        mContext->setNextState(new RendaGameStateEnding(mContext));
    }
}
void RendaGameStateBattle::onUpdateMatrix(){
    updateMatrixForBattle();
}


//RendaGameStateEnding
void RendaGameStateEnding::onBegin(){
}
void RendaGameStateEnding::onEnd(){
}
void RendaGameStateEnding::onUpdateClientCount(size_t inIndex,unsigned char inCount){
    //avoid count up
    mContext->updateClientCount(inIndex,inCount,false);
}
void RendaGameStateEnding::onUpdateMatrix(){
    if(mAnimationCount<FPS*2){
        Adafruit_8x8matrix* matrix=mContext->getMatrix();
        matrix->clear();
        matrix->setRotation(0);
        if(RENDA_MAX<=mContext->getClientTotalCount(0)){
            if(mAnimationCount/4%2==0){
                matrix->drawBitmap(0,0,winner_left_01_bmp,8,8,LED_ON);
            }else{
                matrix->drawBitmap(0,0,winner_left_02_bmp,8,8,LED_ON);
            }
        }else{
            if(mAnimationCount/4%2==0){
                matrix->drawBitmap(0,0,winner_right_01_bmp,8,8,LED_ON);
            }else{
                matrix->drawBitmap(0,0,winner_right_02_bmp,8,8,LED_ON);
            }
        }
        matrix->writeDisplay();
    }else{
        updateMatrixForBattle();
    }
    
    ++mAnimationCount;
    if(FPS*4<mAnimationCount){
        mContext->setNextState(new RendaGameStateTitle(mContext));
    }
}



