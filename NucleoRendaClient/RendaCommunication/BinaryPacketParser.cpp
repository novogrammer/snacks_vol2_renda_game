#include "BinaryPacketParser.h"
#include "PacketConverter.h"

BinaryPacketParser::BinaryPacketParser():TextPacketParserCallbackInterface(),mCallback(NULL){
}

void BinaryPacketParser::assign(BinaryPacketParserCallbackInterface* inCallback){
    mCallback=inCallback;
}

void BinaryPacketParser::onTextPacket(const std::vector<char>& inBuffer){
    std::vector<unsigned char> binaryBuffer;
    PacketConverter::convertTextPacketToBinaryPacket(inBuffer,binaryBuffer);
    if(isValidPacket(binaryBuffer)){
        if(mCallback){
            mCallback->onBinaryPacket(binaryBuffer);
        }
    }
}

bool BinaryPacketParser::isValidPacket(const std::vector<unsigned char>& inBuffer){
    if(inBuffer.size()<sizeof(PacketHeader)){
        return false;
    }
    const PacketHeader *header=(const PacketHeader *)&inBuffer[0];
    if(inBuffer.size()!=header->size){
        return false;
    }
    unsigned char bcc=PacketConverter::calcBCC(&inBuffer[0],inBuffer.size());
    if(bcc!=header->bcc){
        return false;
    }
    return true;
}
