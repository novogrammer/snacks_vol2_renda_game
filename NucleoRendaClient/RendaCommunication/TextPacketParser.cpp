#include "TextPacketParser.h"

const char TextPacketParser::STX=0x02;
const char TextPacketParser::ETX=0x03;


TextPacketParser::TextPacketParser():mBuffer(),mBufferState(BufferState_WaitForSTX),mCallback(NULL){
}

void TextPacketParser::assign(TextPacketParserCallbackInterface* inCallback){
    mCallback=inCallback;
}

void TextPacketParser::write(char inIncommingCharactor){
    switch(mBufferState){
    case BufferState_WaitForSTX:
        if(inIncommingCharactor==STX){
            mBufferState=BufferState_WaitForETX;
        }
        break;
    case BufferState_WaitForETX:
        if(inIncommingCharactor==ETX){
            if(mCallback){
                mCallback->onTextPacket(mBuffer);
            }
            mBuffer.clear();
            mBufferState=BufferState_WaitForSTX;
        }else{
            mBuffer.push_back(inIncommingCharactor);
        }
        break;
    default:
        //DO_NOTHING
        break;
    }
}

