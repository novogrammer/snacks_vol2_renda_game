#ifndef PACKET_CONVERTER_H
#define PACKET_CONVERTER_H
#include "PacketHeader.h"
#include <vector>

//big endian
struct PacketConverter{
    static void makeResponseHeader(const PacketHeader* commandHeader,PacketHeader* replyHeader);
    static void convertTextPacketToBinaryPacket(const std::vector<char>& inTextPacket,std::vector<unsigned char>& outBinaryPacket);
    static void convertBinaryPacketToTextPacket(const std::vector<unsigned char>& inBinaryPacket,std::vector<char>& outTextPacket);
    
    static unsigned char calcBCC(const unsigned char* packet,size_t size){
        const PacketHeader* header=(const PacketHeader*)packet;
        //execute xor operation twice about bcc,instead of skip bcc field
        //bcc xor bcc == 0
        unsigned char bcc=header->bcc;
        for(size_t i=0;i<size;++i){
            unsigned char b=packet[i];
            bcc^=b;
        }
        return bcc;
        
    }
};

#endif//PACKET_CONVERTER_H