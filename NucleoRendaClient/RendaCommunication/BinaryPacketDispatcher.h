#ifndef BINARY_PACKET_DISPATCHER_H
#define BINARY_PACKET_DISPATCHER_H
#include "BinaryPacketParser.h"

struct PacketGetCountCommand;
struct PacketGetCountReply;

struct BinaryPacketDispatcherCallbackInterface{
    virtual void onPacketGetCountCommand(const PacketGetCountCommand& inCommand)=0;
    virtual void onPacketGetCountReply(const PacketGetCountReply& inReply)=0;
};

class BinaryPacketDispatcherTest;
class BinaryPacketDispatcher:public BinaryPacketParserCallbackInterface{
    friend class BinaryPacketDispatcherTest;
    unsigned char mDeviceId;
    BinaryPacketDispatcherCallbackInterface* mCallback;
public:
    BinaryPacketDispatcher(unsigned char inDeviceId);
    void assign(BinaryPacketDispatcherCallbackInterface* inCallback);
    virtual void onBinaryPacket(const std::vector<unsigned char>& inBuffer);
};



#endif//BINARY_PACKET_DISPATCHER_H
