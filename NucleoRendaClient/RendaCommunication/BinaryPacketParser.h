#ifndef BINARY_PACKET_PARSER_H
#define BINARY_PACKET_PARSER_H
#include "TextPacketParser.h"
#include<vector>

struct BinaryPacketParserCallbackInterface{
    virtual void onBinaryPacket(const std::vector<unsigned char>& inBuffer)=0;
};

class BinaryPacketParserTest;

//[STX]text[ETX]
class BinaryPacketParser:public TextPacketParserCallbackInterface{
    friend class BinaryPacketParserTest;
    BinaryPacketParserCallbackInterface* mCallback;
public:
    BinaryPacketParser();
    void assign(BinaryPacketParserCallbackInterface* inCallback);
    virtual void onTextPacket(const std::vector<char>& inBuffer);
private:
    bool isValidPacket(const std::vector<unsigned char>& inBuffer);
};

#endif//BINARY_PACKET_PARSER_H

