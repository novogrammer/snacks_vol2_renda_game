#include "TextPacketParser.h"
#include "PacketHeader.h"
#include "PacketGetCount.h"
#include "PacketConverter.h"
#include "BinaryPacketParser.h"
#include "BinaryPacketDispatcher.h"

#include "common.h"
#include "mbed.h"
#include "MyBuffer.h"

class RendaClient:public BinaryPacketDispatcherCallbackInterface
{
    DigitalIn mButton;
    bool mPrevButtonState;
    unsigned char mButtonCount;
#ifdef USB_DEBUG
    Serial mUSB;
    Timer mDebugTimer;
#endif
    TextPacketParser mTextPacketParser;
    BinaryPacketParser mBinaryPacketParser;
    BinaryPacketDispatcher mBinaryPacketDispatcher;
    Ticker mTicker;
    Ticker mTimeout;
    Serial mNetwork;
    MyBuffer<char> mSendingBuffer;
    MyBuffer<char> mReceivingBuffer;
    enum SendState{
        SendState_Idle,
        SendState_Sending,
    }mSendState;
    DigitalOut mRTS;
    DigitalOut mLED;
public:
    RendaClient();
    void setup();
    void update();
    virtual void onPacketGetCountCommand(const PacketGetCountCommand& inCommand);
    virtual void onPacketGetCountReply(const PacketGetCountReply& inReply);
    void onTick();
    void onReceive();
    void sendGetCountReply(const PacketGetCountCommand& inCommand);
    void receiveIfNeeded();
    void sendIfNeeded();
    void updateButton();
};

