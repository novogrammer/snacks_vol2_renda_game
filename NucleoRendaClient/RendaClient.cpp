#include "RendaClient.h"
#include "PacketGetCount.h"
#include "PacketConverter.h"

RendaClient::RendaClient():
BinaryPacketDispatcherCallbackInterface(),
mButton(BUTTON_NAME),
mPrevButtonState(false),
mButtonCount(),
#ifdef USB_DEBUG
mUSB(USBTX, USBRX,USB_SERIAL_BOUDRATE),
#endif
mTextPacketParser(),
mBinaryPacketParser(),
mBinaryPacketDispatcher(DEVICE_ID),
mTicker(),
mTimeout(),
mNetwork(NETWORK_TX_NAME,NETWORK_RX_NAME,MY_BOUDRATE),
mSendingBuffer(),
mSendState(SendState_Idle),
mRTS(NETWORK_RTS_NAME),
mLED(LED1){
    mTextPacketParser.assign(&mBinaryPacketParser);
    mBinaryPacketParser.assign(&mBinaryPacketDispatcher);
    mBinaryPacketDispatcher.assign(this);
    setup();
}
void RendaClient::setup(){
    mRTS=0;
    mTicker.attach(callback(this,&RendaClient::onTick),1.0/FPS);
    mNetwork.attach(callback(this, &RendaClient::onReceive),Serial::RxIrq);
    mButton.mode(PullUp);
}
void RendaClient::update(){
    receiveIfNeeded();
    sendIfNeeded();
}
void RendaClient::onPacketGetCountCommand(const PacketGetCountCommand& inCommand){
    mLED=!mLED;
    sendGetCountReply(inCommand);
}
void RendaClient::onPacketGetCountReply(const PacketGetCountReply& inReply){
    //DO NOTHING
}

void RendaClient::onTick(){
    updateButton();
}
void RendaClient::onReceive(){
    while(0<mNetwork.readable()){
        int c=mNetwork.getc();
        mReceivingBuffer.put((char)c);
    }
}

void RendaClient::sendGetCountReply(const PacketGetCountCommand& inCommand){
    PacketGetCountReply reply;
    memset(&reply,0,sizeof(reply));
    PacketConverter::makeResponseHeader(&inCommand.header,&reply.header);
    reply.header.size=sizeof(reply);
    reply.count=mButtonCount;
    reply.header.bcc=PacketConverter::calcBCC((unsigned char*)&reply,sizeof(reply));
    std::vector<unsigned char> binaryBuffer;
    binaryBuffer.resize(sizeof(reply));
    memcpy(&binaryBuffer[0],&reply,sizeof(reply));
    
    std::vector<char> textBuffer;
    PacketConverter::convertBinaryPacketToTextPacket(binaryBuffer,textBuffer);
    mSendingBuffer.put(TextPacketParser::STX);
#ifdef USB_DEBUG
    mUSB.puts("S[");
#endif
    for(int i=0;i<textBuffer.size();++i){
        char t=textBuffer[i];
        mSendingBuffer.put(t);
#ifdef USB_DEBUG
        mUSB.putc(t);
#endif
    }
    mSendingBuffer.put(TextPacketParser::ETX);
#ifdef USB_DEBUG
    mUSB.puts("]\r\n");
#endif

}


void RendaClient::receiveIfNeeded(){
    while(mReceivingBuffer.available()){
        char c=mReceivingBuffer.get();
#ifdef USB_DEBUG
        switch(c){
        case 0x02:
            mUSB.puts("R[");
            break;
        case 0x03:
            mUSB.puts("]\r\n");
            break;
        default:
            mUSB.putc(c);
            break;
        }
#endif
        mTextPacketParser.write(c);
    }
}


void RendaClient::sendIfNeeded(){
    if( mNetwork.writeable()){
        switch(mSendState){
        case SendState_Idle:
            if(mSendingBuffer.available()){
                mSendState=SendState_Sending;
                mRTS=1;
#ifdef USB_DEBUG
                mDebugTimer.reset();
                mDebugTimer.start();
#endif
                wait_us(100);//TODO
            }
            break;
        case SendState_Sending:
            if(mSendingBuffer.available()){
                char c=mSendingBuffer.get();
                int result=mNetwork.putc(c);
                //no wait needed in client but..
#ifdef USB_DEBUG
                wait_us(10);
#else
                wait_us(100);
#endif
                //Dont need wait if rxIrq
                //wait_us(100);//OK IF DIABLE SERVER USB_DEBUG
                //wait_us(800);//OK
            }else{
                mSendState=SendState_Idle;
#ifdef USB_DEBUG
                wait_us(100);
#else
#endif
                mRTS=0;
#ifdef USB_DEBUG
                mDebugTimer.stop();
                mUSB.printf("(%f)\r\n",mDebugTimer.read());
#endif

            }
            break;
        default:
            //DO NOTHING
            break;
        }
    }
}

void RendaClient::updateButton(){
    if(mButton!=mPrevButtonState){
        if(!mButton){
            ++mButtonCount;
        }
    }
    mPrevButtonState=mButton;
}


