#ifndef PACKET_GET_COUNT_H
#define PACKET_GET_COUNT_H
#include "PacketHeader.h"
#pragma pack(push)
#pragma pack(1)

#define COMMAND_GET_COUNT 0x02
#define COMMAND_GET_COUNT_REPLY (COMMAND_GET_COUNT | COMMAND_REPLY_MODIFIER)

struct PacketGetCountCommand{
    PacketHeader header;
};
struct PacketGetCountReply{
    PacketHeader header;
    unsigned char count;
};

#pragma pack(pop)

#endif//PACKET_GET_COUNT_H