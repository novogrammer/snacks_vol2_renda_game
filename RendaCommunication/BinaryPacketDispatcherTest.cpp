#include <cppunit/extensions/HelperMacros.h>
#include "BinaryPacketDispatcher.h"
#include "PacketHeader.h"
#include "PacketGetCount.h"

struct BinaryPacketDispatcherCallbackInterfaceStub:public BinaryPacketDispatcherCallbackInterface{
    std::vector<PacketGetCountCommand> onPacketGetCountCommand_inCommandList;
    std::vector<PacketGetCountReply> onPacketGetCountReply_inReplyList;
    void onPacketGetCountCommand(const PacketGetCountCommand& inCommand){
        onPacketGetCountCommand_inCommandList.push_back(inCommand);
    }
    void onPacketGetCountReply(const PacketGetCountReply& inReply){
        onPacketGetCountReply_inReplyList.push_back(inReply);
    }
};


class BinaryPacketDispatcherTest:public CPPUNIT_NS::TestFixture{
    CPPUNIT_TEST_SUITE(BinaryPacketDispatcherTest);
    CPPUNIT_TEST(test_constructor);
    CPPUNIT_TEST(test_assign);
    CPPUNIT_TEST(test_onBinaryPacket);
    CPPUNIT_TEST_SUITE_END();
    BinaryPacketDispatcherCallbackInterfaceStub *stub;
    BinaryPacketDispatcher *target;
public:
    void setUp();
    void tearDown();
protected:
    void test_constructor();
    void test_assign();
    void test_onBinaryPacket();
};



void BinaryPacketDispatcherTest::setUp(){
    stub=new BinaryPacketDispatcherCallbackInterfaceStub();
    target=new BinaryPacketDispatcher(0x12);
    target->assign(stub);
}
void BinaryPacketDispatcherTest::tearDown(){
    delete target;
    target=NULL;
    delete stub;
    stub=NULL;
}
void BinaryPacketDispatcherTest::test_constructor(){
    CPPUNIT_ASSERT_EQUAL((unsigned char)0x12,target->mDeviceId);
    CPPUNIT_ASSERT_EQUAL((BinaryPacketDispatcherCallbackInterface*)stub,target->mCallback);
}
void BinaryPacketDispatcherTest::test_assign(){
    CPPUNIT_ASSERT_EQUAL((BinaryPacketDispatcherCallbackInterface*)stub,target->mCallback);
    target->assign(NULL);
    CPPUNIT_ASSERT_EQUAL((BinaryPacketDispatcherCallbackInterface*)NULL,target->mCallback);
}


void BinaryPacketDispatcherTest::test_onBinaryPacket(){
    {
        PacketGetCountCommand command;
        memset(&command,0,sizeof(command));
        command.header.to=0x12;
        command.header.command=COMMAND_GET_COUNT;
        std::vector<unsigned char> binaryPacket;
        binaryPacket.resize(sizeof(command));
        memcpy(&binaryPacket[0],&command,sizeof(command));
        CPPUNIT_ASSERT_EQUAL((size_t)0,stub->onPacketGetCountCommand_inCommandList.size());
        target->onBinaryPacket(binaryPacket);
        CPPUNIT_ASSERT_EQUAL((size_t)1,stub->onPacketGetCountCommand_inCommandList.size());
        if(memcmp(&command,&(stub->onPacketGetCountCommand_inCommandList[0]),sizeof(command))!=0){
            CPPUNIT_FAIL("memcmp(&command,&(stub->onPacketGetCountCommand_inCommandList[0]),sizeof(command))!=0");
        }
    }
    {
        PacketGetCountReply reply;
        memset(&reply,0,sizeof(reply));
        reply.header.to=0x12;
        reply.header.command=COMMAND_GET_COUNT_REPLY;
        std::vector<unsigned char> binaryPacket;
        binaryPacket.resize(sizeof(reply));
        memcpy(&binaryPacket[0],&reply,sizeof(reply));
        CPPUNIT_ASSERT_EQUAL((size_t)0,stub->onPacketGetCountReply_inReplyList.size());
        target->onBinaryPacket(binaryPacket);
        CPPUNIT_ASSERT_EQUAL((size_t)1,stub->onPacketGetCountReply_inReplyList.size());
        if(memcmp(&reply,&(stub->onPacketGetCountReply_inReplyList[0]),sizeof(reply))!=0){
            CPPUNIT_FAIL("memcmp(&reply,&(stub->onPacketGetCountReply_inReplyList[0]),sizeof(reply))!=0");
        }
    }

}


CPPUNIT_TEST_SUITE_REGISTRATION(BinaryPacketDispatcherTest);

