#include "BinaryPacketDispatcher.h"
#include "PacketHeader.h"
#include "PacketGetCount.h"

BinaryPacketDispatcher::BinaryPacketDispatcher(unsigned char inDeviceId):BinaryPacketParserCallbackInterface(),mDeviceId(inDeviceId),mCallback(NULL){
    
}
void BinaryPacketDispatcher::assign(BinaryPacketDispatcherCallbackInterface* inCallback){
    mCallback=inCallback;
}
void BinaryPacketDispatcher::onBinaryPacket(const std::vector<unsigned char>& inBuffer){
    const PacketHeader* header=(PacketHeader*)&inBuffer[0];
    if(!mCallback){
        return;
    }
    if(header->to!=mDeviceId){
        return;
    }
    switch(header->command){
    case COMMAND_GET_COUNT:
        if(sizeof(PacketGetCountCommand)==inBuffer.size()){
            const PacketGetCountCommand* command=(PacketGetCountCommand*)&inBuffer[0];
            mCallback->onPacketGetCountCommand(*command);
        }
        break;
    case COMMAND_GET_COUNT_REPLY:
        if(sizeof(PacketGetCountReply)==inBuffer.size()){
            const PacketGetCountReply* reply=(PacketGetCountReply*)&inBuffer[0];
            mCallback->onPacketGetCountReply(*reply);
        }
        break;
    default:
        //DO NOTHING
        break;
    }
}

