#include <cppunit/extensions/HelperMacros.h>
#include "TextPacketParser.h"

struct TextPacketParserCallbackInterfaceStub:public TextPacketParserCallbackInterface{
    std::vector<char> onTextPacket_inBuffer;
    void onTextPacket(const std::vector<char>& inBuffer){
        onTextPacket_inBuffer=inBuffer;
    }
};


class TextPacketParserTest:public CPPUNIT_NS::TestFixture{
    CPPUNIT_TEST_SUITE(TextPacketParserTest);
    CPPUNIT_TEST(test_constructor);
    CPPUNIT_TEST(test_assign);
    CPPUNIT_TEST(test_write);
    CPPUNIT_TEST_SUITE_END();
    TextPacketParserCallbackInterfaceStub *stub;
    TextPacketParser *target;
public:
    void setUp();
    void tearDown();
protected:
    void test_constructor();
    void test_assign();
    void test_write();
};



void TextPacketParserTest::setUp(){
    stub=new TextPacketParserCallbackInterfaceStub();
    target=new TextPacketParser();
    target->assign(stub);
}
void TextPacketParserTest::tearDown(){
    delete target;
    target=NULL;
    delete stub;
    stub=NULL;
}
void TextPacketParserTest::test_constructor(){
    CPPUNIT_ASSERT_EQUAL((TextPacketParserCallbackInterface*)stub,target->mCallback);
    CPPUNIT_ASSERT_EQUAL(TextPacketParser::BufferState_WaitForSTX,target->mBufferState);
    {
        std::vector<char> expect;
        if(target->mBuffer!=expect){
            CPPUNIT_FAIL("target->mBuffer!=expect");
        }
    }
}
void TextPacketParserTest::test_assign(){
    CPPUNIT_ASSERT_EQUAL((TextPacketParserCallbackInterface*)stub,target->mCallback);
    target->assign(NULL);
    CPPUNIT_ASSERT_EQUAL((TextPacketParserCallbackInterface*)NULL,target->mCallback);
}
void TextPacketParserTest::test_write(){
    {
        std::vector<char> expect;
        if(stub->onTextPacket_inBuffer!=expect){
            CPPUNIT_FAIL("stub->onTextPacket_inBuffer!=expect");
        }
    }
    {
        CPPUNIT_ASSERT_EQUAL(TextPacketParser::BufferState_WaitForSTX,target->mBufferState);
        target->write(0x02);
        CPPUNIT_ASSERT_EQUAL(TextPacketParser::BufferState_WaitForETX,target->mBufferState);
        target->write('a');
        target->write('b');
        target->write('c');
        CPPUNIT_ASSERT_EQUAL(TextPacketParser::BufferState_WaitForETX,target->mBufferState);
        target->write(0x03);
        CPPUNIT_ASSERT_EQUAL(TextPacketParser::BufferState_WaitForSTX,target->mBufferState);
        std::vector<char> expect;
        expect.push_back('a');
        expect.push_back('b');
        expect.push_back('c');
        if(stub->onTextPacket_inBuffer!=expect){
            CPPUNIT_FAIL("stub->onTextPacket_inBuffer!=expect");
        }
    }
    {
        target->write('x');
        CPPUNIT_ASSERT_EQUAL(TextPacketParser::BufferState_WaitForSTX,target->mBufferState);
        target->write(0x02);
        CPPUNIT_ASSERT_EQUAL(TextPacketParser::BufferState_WaitForETX,target->mBufferState);
        target->write('A');
        target->write('B');
        target->write('C');
        CPPUNIT_ASSERT_EQUAL(TextPacketParser::BufferState_WaitForETX,target->mBufferState);
        target->write(0x03);
        CPPUNIT_ASSERT_EQUAL(TextPacketParser::BufferState_WaitForSTX,target->mBufferState);
        target->write('x');
        std::vector<char> expect;
        expect.push_back('A');
        expect.push_back('B');
        expect.push_back('C');
        if(stub->onTextPacket_inBuffer!=expect){
            CPPUNIT_FAIL("stub->onTextPacket_inBuffer!=expect");
        }
    }
  
}
CPPUNIT_TEST_SUITE_REGISTRATION(TextPacketParserTest);

