#include <cppunit/extensions/HelperMacros.h>
#include "PacketConverter.h"

class PacketConverterTest:public CPPUNIT_NS::TestFixture{
    CPPUNIT_TEST_SUITE(PacketConverterTest);
    CPPUNIT_TEST(test_makeResponseHeader);
    CPPUNIT_TEST(test_convertTextPacketToBinaryPacket);
    CPPUNIT_TEST(test_convertBinaryPacketToTextPacket);
    CPPUNIT_TEST(test_calcBCC);
    CPPUNIT_TEST_SUITE_END();
public:
    void setUp();
    void tearDown();
protected:
    void test_makeResponseHeader();
    void test_convertTextPacketToBinaryPacket();
    void test_convertBinaryPacketToTextPacket();
    void test_calcBCC();
};



void PacketConverterTest::setUp(){
}
void PacketConverterTest::tearDown(){
}

void PacketConverterTest::test_makeResponseHeader(){
    PacketHeader command;
    PacketHeader reply;
    if(sizeof(command)!=5){
        CPPUNIT_FAIL("sizeof(command)!=5");
    }
    memset(&command,0,sizeof(command));
    memset(&reply,0,sizeof(command));
    command.from=0x01;
    command.to=0x02;
    command.command=0x02;
    command.size=0x03;
    command.bcc=0x04;
    PacketConverter::makeResponseHeader(&command,&reply);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0x02,reply.from);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0x01,reply.to);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0x03,reply.command);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0x00,reply.size);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0x00,reply.bcc);
}

void PacketConverterTest::test_convertTextPacketToBinaryPacket(){
    std::vector<char> textPacket;
    std::vector<unsigned char> binaryPacket;
    textPacket.push_back('0');
    textPacket.push_back('1');
    textPacket.push_back('2');
    textPacket.push_back('3');
    textPacket.push_back('4');
    textPacket.push_back('5');
    textPacket.push_back('6');
    textPacket.push_back('7');
    textPacket.push_back('8');
    textPacket.push_back('9');
    textPacket.push_back('a');
    textPacket.push_back('b');
    textPacket.push_back('c');
    textPacket.push_back('d');
    textPacket.push_back('e');
    textPacket.push_back('f');
    textPacket.push_back('A');
    textPacket.push_back('B');
    textPacket.push_back('C');
    textPacket.push_back('D');
    textPacket.push_back('E');
    textPacket.push_back('F');
    textPacket.push_back('X');//invalid char
    textPacket.push_back('X');//invalid char
    textPacket.push_back('f');//fragment
    
    PacketConverter::convertTextPacketToBinaryPacket(textPacket,binaryPacket);
    
    CPPUNIT_ASSERT_EQUAL((size_t)(8+3+1+1),binaryPacket.size());
    CPPUNIT_ASSERT_EQUAL((unsigned char)0x01,binaryPacket[0]);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0x23,binaryPacket[1]);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0x45,binaryPacket[2]);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0x67,binaryPacket[3]);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0x89,binaryPacket[4]);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0xab,binaryPacket[5]);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0xcd,binaryPacket[6]);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0xef,binaryPacket[7]);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0xab,binaryPacket[8]);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0xcd,binaryPacket[9]);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0xef,binaryPacket[10]);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0x00,binaryPacket[11]);
    CPPUNIT_ASSERT_EQUAL((unsigned char)0xf0,binaryPacket[12]);
  
}
void PacketConverterTest::test_convertBinaryPacketToTextPacket(){
    std::vector<unsigned char> binaryPacket;
    std::vector<char> textPacket;
    binaryPacket.push_back(0x01);
    binaryPacket.push_back(0x23);
    binaryPacket.push_back(0x45);
    binaryPacket.push_back(0x67);
    binaryPacket.push_back(0x89);
    binaryPacket.push_back(0xab);
    binaryPacket.push_back(0xcd);
    binaryPacket.push_back(0xef);
    
    PacketConverter::convertBinaryPacketToTextPacket(binaryPacket,textPacket);
    
    CPPUNIT_ASSERT_EQUAL((size_t)(16),textPacket.size());
    CPPUNIT_ASSERT_EQUAL('0',textPacket[0]);
    CPPUNIT_ASSERT_EQUAL('1',textPacket[1]);
    CPPUNIT_ASSERT_EQUAL('2',textPacket[2]);
    CPPUNIT_ASSERT_EQUAL('3',textPacket[3]);
    CPPUNIT_ASSERT_EQUAL('4',textPacket[4]);
    CPPUNIT_ASSERT_EQUAL('5',textPacket[5]);
    CPPUNIT_ASSERT_EQUAL('6',textPacket[6]);
    CPPUNIT_ASSERT_EQUAL('7',textPacket[7]);
    CPPUNIT_ASSERT_EQUAL('8',textPacket[8]);
    CPPUNIT_ASSERT_EQUAL('9',textPacket[9]);
    CPPUNIT_ASSERT_EQUAL('a',textPacket[10]);
    CPPUNIT_ASSERT_EQUAL('b',textPacket[11]);
    CPPUNIT_ASSERT_EQUAL('c',textPacket[12]);
    CPPUNIT_ASSERT_EQUAL('d',textPacket[13]);
    CPPUNIT_ASSERT_EQUAL('e',textPacket[14]);
    CPPUNIT_ASSERT_EQUAL('f',textPacket[15]);
}


void PacketConverterTest::test_calcBCC(){
    std::vector<unsigned char> validBinaryPacket;
    {
        PacketHeader header;
        memset(&header,0,sizeof(header));
        header.from=0x01;
        header.to=0x02;
        header.command=0x04;
        header.size=sizeof(header);//=0x05
        header.bcc=0x02;//= 0x01 ^ 0x02 ^ 0x04 ^ 0x05
        validBinaryPacket.resize(sizeof(header));
        memcpy(&validBinaryPacket[0],&header,sizeof(header));

        unsigned char bcc=PacketConverter::calcBCC(&validBinaryPacket[0],validBinaryPacket.size());
        CPPUNIT_ASSERT_EQUAL((int)0x02,(int)bcc);
    }
    {
        std::vector<unsigned char> invalidBinaryPacketWrongBcc=validBinaryPacket;
        PacketHeader *header=(PacketHeader*)&invalidBinaryPacketWrongBcc[0];
        header->bcc=0xaa;//dont care
        unsigned char bcc=PacketConverter::calcBCC(&invalidBinaryPacketWrongBcc[0],invalidBinaryPacketWrongBcc.size());
        
        CPPUNIT_ASSERT_EQUAL((int)0x02,(int)bcc);
    }
}


CPPUNIT_TEST_SUITE_REGISTRATION(PacketConverterTest);

