#ifndef TEXT_PACKET_HEADER_H
#define TEXT_PACKET_HEADER_H
#pragma pack(push)
#pragma pack(1)

#define COMMAND_REPLY_MODIFIER 0x01


struct PacketHeader{
    unsigned char from;
    unsigned char to;
    unsigned char command;
    //total packet size
    unsigned char size;
    //Block Check Charactor
    //xor all bytes.
    //fill zero before block check.
    unsigned char bcc;
};

#pragma pack(pop)

#endif//TEXT_PACKET_HEADER_H

