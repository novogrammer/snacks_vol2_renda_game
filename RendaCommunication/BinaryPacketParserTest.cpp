#include <cppunit/extensions/HelperMacros.h>
#include "BinaryPacketParser.h"
#include "PacketHeader.h"
#include "PacketConverter.h"

struct BinaryPacketParserCallbackInterfaceStub:public BinaryPacketParserCallbackInterface{
    std::vector<unsigned char> onBinaryPacket_inBuffer;
    void onBinaryPacket(const std::vector<unsigned char>& inBuffer){
        onBinaryPacket_inBuffer=inBuffer;
    }
};


class BinaryPacketParserTest:public CPPUNIT_NS::TestFixture{
    CPPUNIT_TEST_SUITE(BinaryPacketParserTest);
    CPPUNIT_TEST(test_constructor);
    CPPUNIT_TEST(test_assign);
    CPPUNIT_TEST(test_onTextPacket);
    CPPUNIT_TEST(test_isValidPacket);
    CPPUNIT_TEST_SUITE_END();
    BinaryPacketParserCallbackInterfaceStub *stub;
    BinaryPacketParser *target;
public:
    void setUp();
    void tearDown();
protected:
    void test_constructor();
    void test_assign();
    void test_onTextPacket();
    void test_isValidPacket();
};



void BinaryPacketParserTest::setUp(){
    stub=new BinaryPacketParserCallbackInterfaceStub();
    target=new BinaryPacketParser();
    target->assign(stub);
}
void BinaryPacketParserTest::tearDown(){
    delete target;
    target=NULL;
    delete stub;
    stub=NULL;
}
void BinaryPacketParserTest::test_constructor(){
    CPPUNIT_ASSERT_EQUAL((BinaryPacketParserCallbackInterface*)stub,target->mCallback);
}
void BinaryPacketParserTest::test_assign(){
    CPPUNIT_ASSERT_EQUAL((BinaryPacketParserCallbackInterface*)stub,target->mCallback);
    target->assign(NULL);
    CPPUNIT_ASSERT_EQUAL((BinaryPacketParserCallbackInterface*)NULL,target->mCallback);
}


void BinaryPacketParserTest::test_onTextPacket(){
    std::vector<unsigned char> validBinaryPacket;
    std::vector<char> validTextPacket;
    {
        PacketHeader header;
        memset(&header,0,sizeof(header));
        header.from=0x01;
        header.to=0x02;
        header.command=0x04;
        header.size=sizeof(header);//=0x05
        header.bcc=0x02;//= 0x01 ^ 0x02 ^ 0x04 ^ 0x05
        validBinaryPacket.resize(sizeof(header));
        memcpy(&validBinaryPacket[0],&header,sizeof(header));

        PacketConverter::convertBinaryPacketToTextPacket(validBinaryPacket,validTextPacket);
    }
    target->onTextPacket(validTextPacket);
    if(stub->onBinaryPacket_inBuffer!=validBinaryPacket){
        CPPUNIT_FAIL("stub->onBinaryPacket_inBuffer!=validBinaryPacket");
    }
    stub->onBinaryPacket_inBuffer.clear();
    
    std::vector<unsigned char> invalidBinaryPacketWrongBcc;
    std::vector<char> invalidTextPacketWrongBcc;
    {
        invalidBinaryPacketWrongBcc=validBinaryPacket;
        PacketHeader *header=(PacketHeader*)&invalidBinaryPacketWrongBcc[0];
        header->bcc+=1;
        PacketConverter::convertBinaryPacketToTextPacket(invalidBinaryPacketWrongBcc,invalidTextPacketWrongBcc);
    }
    target->onTextPacket(invalidTextPacketWrongBcc);
    CPPUNIT_ASSERT_EQUAL((size_t)0,stub->onBinaryPacket_inBuffer.size());
}

void BinaryPacketParserTest::test_isValidPacket(){
    std::vector<unsigned char> validPacket;
    {
        PacketHeader header;
        memset(&header,0,sizeof(header));
        header.from=0x01;
        header.to=0x02;
        header.command=0x04;
        header.size=sizeof(header);//=0x05
        header.bcc=0x02;//= 0x01 ^ 0x02 ^ 0x04 ^ 0x05
        validPacket.resize(sizeof(header));
        memcpy(&validPacket[0],&header,sizeof(header));
    }
    CPPUNIT_ASSERT_EQUAL(true,target->isValidPacket(validPacket));
    std::vector<unsigned char> invalidPacketTooShort;
    {
        invalidPacketTooShort=validPacket;
        PacketHeader *header=(PacketHeader*)&invalidPacketTooShort[0];
        header->size-=1;
        invalidPacketTooShort.pop_back();
    }
    CPPUNIT_ASSERT_EQUAL(false,target->isValidPacket(invalidPacketTooShort));
    std::vector<unsigned char> invalidPacketWrongSize;
    {
        invalidPacketWrongSize=validPacket;
        PacketHeader *header=(PacketHeader*)&invalidPacketWrongSize[0];
        header->size+=1;
        header->bcc=0x01;//= 0x01 ^ 0x02 ^ 0x04 ^ 0x06
    }
    CPPUNIT_ASSERT_EQUAL(false,target->isValidPacket(invalidPacketWrongSize));
    std::vector<unsigned char> invalidPacketWrongBcc;
    {
        invalidPacketWrongBcc=validPacket;
        PacketHeader *header=(PacketHeader*)&invalidPacketWrongBcc[0];
        header->bcc+=1;
    }
    CPPUNIT_ASSERT_EQUAL(false,target->isValidPacket(invalidPacketWrongBcc));
}

CPPUNIT_TEST_SUITE_REGISTRATION(BinaryPacketParserTest);

